import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const timeout = ref(2000);
  const message = ref("");
  const showMessag = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };
  const closemessage = () => {
    message.value = "";
    isShow.value = false;
  };

  return { isShow, message, showMessag, closemessage, timeout };
});
